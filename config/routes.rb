Rails.application.routes.draw do
  defaults format: :json do
    resources :weeks, only: %i[create update]
  end
end
