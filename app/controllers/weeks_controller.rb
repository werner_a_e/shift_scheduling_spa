class WeeksController < ApplicationController
  def show
    week = Week.find(params[:id])

    respond_to do |format|
      format.json { render json: week, status: :ok }
    end
  end

  def create
    week = Week.new(week_params_for_create)

    respond_to do |format|
      if week.save
        format.json { render json: week, status: :created }
      else
        format.json { render json: week.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    week = Week.find(params[:id])

    respond_to do |format|
      if week.update(week_params_for_update)
        format.json { render json: week.scheduler, status: :ok }
      else
        format.json { render json: week.errors, status: :unprocessable_entity }
      end
    end
  end

  private

  def week_params_for_create
    params.require(:week).permit(:start_at, :contract_id)
  end

  def week_params_for_update
    params.require(:week).permit(applied_shifts_attributes: %i[person_id shift_id])
  end
end
