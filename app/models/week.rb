class Week < ApplicationRecord
  belongs_to :contract
  has_many :applied_shifts
  accepts_nested_attributes_for :applied_shifts

  def scheduler
    assigned = []
    applied_shifts.shuffle.group_by(&:person_id).each do |_, grouped_applied_shifts|
      grouped_applied_shifts.each do |applied_shift|
        next if applied_shifts
                .where(shift_id: applied_shift.shift_id).count > 1 &&
                assigned.find do |applied_shift_assigned|
                  applied_shift_assigned.shift_id == applied_shift.shift_id
                end.present?

        assigned << applied_shift
      end
    end
    assigned
  end
end
