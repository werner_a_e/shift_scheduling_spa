class AppliedShift < ApplicationRecord
  belongs_to :person
  belongs_to :shift
  belongs_to :week
end
