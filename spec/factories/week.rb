FactoryBot.define do
  factory :week do
    start_at { Date.new(2020, 10, 1) }
    contract
  end
end
