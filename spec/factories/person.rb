FactoryBot.define do
  factory :person do
    full_name { Faker::Name.name }
  end

  factory :peter, class: Person do
    full_name { 'Peter Parker' }
  end

  factory :jhon, class: Person do
    full_name { 'Jhon Doe' }
  end

  factory :goku, class: Person do
    full_name { 'Son Goku' }
  end
end