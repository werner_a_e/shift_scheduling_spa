require 'rails_helper'

RSpec.describe '/weeks', type: :request do
  let(:contract) { create(:contract) }

  describe 'POST /create' do
    it 'creates a new Week on successful request' do
      expect do
        post weeks_url,
             params: { week: { start_at: Date.new(2020, 10, 1), contract_id: contract.id } }
      end.to(
        change(Week, :count).by(1)
      )
      expect(response).to have_http_status(:created)
    end

    it 'response with unprocessable entity on unsuccessful request' do
      expect do
        post weeks_url, params: { week: { something: 'invalid' } }
      end.not_to(
        change { Week.count }
      )
      expect(response).to have_http_status(:unprocessable_entity)
    end
  end

  describe 'PUT /update' do
    it 'does not update contract or week' do
      week = create(:week)
      put week_url(week),
          params: {
            week: {
              start_at: Date.new(2021, 2, 1), contract_id: create(:contract, name: 'Another Contract').id
            }
          }
      expect(response).to have_http_status(:ok)
      expect(week.reload.start_at).to eq(Date.new(2020, 10, 1))
      expect(week.reload.contract.name).to eq(contract.name)
    end

    it 'updates a Week' do
      week = create(:week)
      expect(week.applied_shifts).to be_empty
      jhon = create(:jhon)
      peter = create(:peter)
      goku = create(:goku)

      jhon_assignments =
        4.times.map do |n|
          first_shift = Shift.where(day_of_week: 1, starts_at: 19 + n).first
          { person_id: jhon.id, shift_id: first_shift.id }
        end

      peter_assignments =
        6.times.map do |n|
          second_shift = Shift.where(day_of_week: 6, starts_at: 10 + n).first
          { person_id: peter.id, shift_id: second_shift.id }
        end

      goku_assignments =
        5.times.map do |n|
          third_shift = Shift.where(day_of_week: 1, starts_at: 19 + n).first
          { person_id: goku.id, shift_id: third_shift.id }
        end

      put week_url(week),
          params: {
            week: {
              applied_shifts_attributes: jhon_assignments + peter_assignments + goku_assignments
            }
          }
      week.reload
      expect(response).to have_http_status(:ok)
      expect(response.body).to(
        include("\"person_id\":#{jhon.id},\"shift_id\":#{Shift.where(day_of_week: 1, starts_at: 19).first.id}").or(
          include("\"person_id\":#{goku.id},\"shift_id\":#{Shift.where(day_of_week: 1, starts_at: 23).first.id}")
        )
      )
      expect(response.body).to(
        include("\"person_id\":#{peter.id},\"shift_id\":#{Shift.where(day_of_week: 6, starts_at: 10).first.id}")
      )
    end
  end
end
