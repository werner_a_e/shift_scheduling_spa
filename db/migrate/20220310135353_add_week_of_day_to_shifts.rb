class AddWeekOfDayToShifts < ActiveRecord::Migration[7.0]
  def change
    add_column :shifts, :day_of_week, :integer
  end
end
