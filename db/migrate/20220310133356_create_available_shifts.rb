class CreateAvailableShifts < ActiveRecord::Migration[7.0]
  def change
    create_table :available_shifts do |t|
      t.references :person, foreign_key: true
      t.references :shift, foreign_key: true
      t.references :week, foreign_key: true

      t.timestamps
    end
  end
end
