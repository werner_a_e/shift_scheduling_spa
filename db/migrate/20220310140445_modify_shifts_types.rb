class ModifyShiftsTypes < ActiveRecord::Migration[7.0]
  def change
    change_column :shifts, :starts_at, :integer
    change_column :shifts, :ends_at, :integer
  end
end
