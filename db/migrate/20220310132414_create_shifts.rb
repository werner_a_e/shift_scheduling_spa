class CreateShifts < ActiveRecord::Migration[7.0]
  def change
    create_table :shifts do |t|
      t.time :starts_at
      t.time :ends_at

      t.timestamps
    end
  end
end
