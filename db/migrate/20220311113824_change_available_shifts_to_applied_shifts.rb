class ChangeAvailableShiftsToAppliedShifts < ActiveRecord::Migration[7.0]
  def change
    rename_table :available_shifts, :applied_shifts
  end
end
