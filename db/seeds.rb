# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: "Star Wars" }, { name: "Lord of the Rings" }])
#   Character.create(name: "Luke", movie: movies.first)
ActiveRecord::Base.transaction do
  Contract.create(name: 'Recorrido.cl')
  Contract.create(name: 'Recorrido.pe')

  1.upto(5) do |day_of_week|
    19.upto(23) do |start_shift|
      Shift.create(day_of_week: day_of_week, starts_at: start_shift, ends_at: start_shift + 1)
    end
  end

  6.upto(7) do |day_of_week|
    10.upto(23) do |start_shift|
      Shift.create(day_of_week: day_of_week, starts_at: start_shift, ends_at: start_shift + 1)
    end
  end
end
