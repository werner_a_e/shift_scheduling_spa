# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.0].define(version: 2022_03_11_113824) do
  create_table "applied_shifts", force: :cascade do |t|
    t.integer "person_id"
    t.integer "shift_id"
    t.integer "week_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["person_id"], name: "index_applied_shifts_on_person_id"
    t.index ["shift_id"], name: "index_applied_shifts_on_shift_id"
    t.index ["week_id"], name: "index_applied_shifts_on_week_id"
  end

  create_table "contracts", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "people", force: :cascade do |t|
    t.string "full_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "shifts", force: :cascade do |t|
    t.integer "starts_at"
    t.integer "ends_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "day_of_week"
  end

  create_table "weeks", force: :cascade do |t|
    t.date "start_at"
    t.integer "contract_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["contract_id"], name: "index_weeks_on_contract_id"
  end

  add_foreign_key "applied_shifts", "people"
  add_foreign_key "applied_shifts", "shifts"
  add_foreign_key "applied_shifts", "weeks"
  add_foreign_key "weeks", "contracts"
end
